package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class BusinessLogic {
    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessLogic.class);

    public boolean doLogic(boolean flag) {
        if (flag) {
            LOGGER.info("awesome");
        } else {
            LOGGER.info("cool");
        }
        return flag;
    }
}
