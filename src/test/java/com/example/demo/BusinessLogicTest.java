package com.example.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BusinessLogicTest {
    @Test
    void test() {
        BusinessLogic logic = new BusinessLogic();
        Assertions.assertTrue(logic.doLogic(true));
    }

    @Test
    void test2() {
        BusinessLogic logic = new BusinessLogic();
        Assertions.assertFalse(logic.doLogic(false));
    }
}